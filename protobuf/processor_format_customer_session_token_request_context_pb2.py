# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: processor_format_customer_session_token_request_context.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n=processor_format_customer_session_token_request_context.proto\x12\tprocessor\"\xa4\x01\n1ProcessorFormatCustomerSessionTokenRequestContext\x12\r\n\x05_type\x18\x01 \x01(\t\x12\x11\n\treference\x18\x02 \x01(\t\x12\x13\n\x0bmerchant_id\x18\x03 \x01(\t\x12\x15\n\rcurrency_code\x18\x04 \x01(\t\x12\x10\n\x08language\x18\x05 \x01(\t\x12\x0f\n\x07\x63ountry\x18\x06 \x01(\tb\x06proto3')



_PROCESSORFORMATCUSTOMERSESSIONTOKENREQUESTCONTEXT = DESCRIPTOR.message_types_by_name['ProcessorFormatCustomerSessionTokenRequestContext']
ProcessorFormatCustomerSessionTokenRequestContext = _reflection.GeneratedProtocolMessageType('ProcessorFormatCustomerSessionTokenRequestContext', (_message.Message,), {
  'DESCRIPTOR' : _PROCESSORFORMATCUSTOMERSESSIONTOKENREQUESTCONTEXT,
  '__module__' : 'processor_format_customer_session_token_request_context_pb2'
  # @@protoc_insertion_point(class_scope:processor.ProcessorFormatCustomerSessionTokenRequestContext)
  })
_sym_db.RegisterMessage(ProcessorFormatCustomerSessionTokenRequestContext)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _PROCESSORFORMATCUSTOMERSESSIONTOKENREQUESTCONTEXT._serialized_start=77
  _PROCESSORFORMATCUSTOMERSESSIONTOKENREQUESTCONTEXT._serialized_end=241
# @@protoc_insertion_point(module_scope)
