# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: processor_format_authorization_request_context.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n4processor_format_authorization_request_context.proto\x12\tprocessor\"\xad\x04\n*ProcessorFormatAuthorizationRequestContext\x12\r\n\x05_type\x18\x01 \x01(\t\x12[\n\x0b\x63redentials\x18\x02 \x03(\x0b\x32\x46.processor.ProcessorFormatAuthorizationRequestContext.CredentialsEntry\x12\x1a\n\x12merchant_reference\x18\x03 \x01(\t\x12\x1c\n\x14statement_descriptor\x18\x04 \x01(\t\x12\x1b\n\x13merchant_account_id\x18\x05 \x01(\t\x12\x0e\n\x06\x61mount\x18\x06 \x01(\x03\x12\x15\n\rcurrency_code\x18\x07 \x01(\t\x12\x17\n\x0f\x62illing_address\x18\x08 \x01(\t\x12 \n\x18payment_instrument_token\x18\t \x01(\t\x12!\n\x19recurring_payment_options\x18\n \x01(\t\x12S\n\x07mapping\x18\x0b \x03(\x0b\x32\x42.processor.ProcessorFormatAuthorizationRequestContext.MappingEntry\x1a\x32\n\x10\x43redentialsEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t:\x02\x38\x01\x1a.\n\x0cMappingEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t:\x02\x38\x01\x62\x06proto3')



_PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT = DESCRIPTOR.message_types_by_name['ProcessorFormatAuthorizationRequestContext']
_PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_CREDENTIALSENTRY = _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT.nested_types_by_name['CredentialsEntry']
_PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_MAPPINGENTRY = _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT.nested_types_by_name['MappingEntry']
ProcessorFormatAuthorizationRequestContext = _reflection.GeneratedProtocolMessageType('ProcessorFormatAuthorizationRequestContext', (_message.Message,), {

  'CredentialsEntry' : _reflection.GeneratedProtocolMessageType('CredentialsEntry', (_message.Message,), {
    'DESCRIPTOR' : _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_CREDENTIALSENTRY,
    '__module__' : 'processor_format_authorization_request_context_pb2'
    # @@protoc_insertion_point(class_scope:processor.ProcessorFormatAuthorizationRequestContext.CredentialsEntry)
    })
  ,

  'MappingEntry' : _reflection.GeneratedProtocolMessageType('MappingEntry', (_message.Message,), {
    'DESCRIPTOR' : _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_MAPPINGENTRY,
    '__module__' : 'processor_format_authorization_request_context_pb2'
    # @@protoc_insertion_point(class_scope:processor.ProcessorFormatAuthorizationRequestContext.MappingEntry)
    })
  ,
  'DESCRIPTOR' : _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT,
  '__module__' : 'processor_format_authorization_request_context_pb2'
  # @@protoc_insertion_point(class_scope:processor.ProcessorFormatAuthorizationRequestContext)
  })
_sym_db.RegisterMessage(ProcessorFormatAuthorizationRequestContext)
_sym_db.RegisterMessage(ProcessorFormatAuthorizationRequestContext.CredentialsEntry)
_sym_db.RegisterMessage(ProcessorFormatAuthorizationRequestContext.MappingEntry)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_CREDENTIALSENTRY._options = None
  _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_CREDENTIALSENTRY._serialized_options = b'8\001'
  _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_MAPPINGENTRY._options = None
  _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_MAPPINGENTRY._serialized_options = b'8\001'
  _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT._serialized_start=68
  _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT._serialized_end=625
  _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_CREDENTIALSENTRY._serialized_start=527
  _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_CREDENTIALSENTRY._serialized_end=577
  _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_MAPPINGENTRY._serialized_start=579
  _PROCESSORFORMATAUTHORIZATIONREQUESTCONTEXT_MAPPINGENTRY._serialized_end=625
# @@protoc_insertion_point(module_scope)
