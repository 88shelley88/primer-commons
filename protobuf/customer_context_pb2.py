# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: customer_context.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x16\x63ustomer_context.proto\x12\tprocessor\"\x7f\n\x0f\x43ustomerContext\x12\r\n\x05_type\x18\x01 \x01(\t\x12\x13\n\x0b\x63ustomer_id\x18\x02 \x01(\t\x12\x15\n\remail_address\x18\x03 \x01(\t\x12\x17\n\x0f\x62illing_address\x18\x04 \x01(\t\x12\x18\n\x10shipping_address\x18\x05 \x01(\tb\x06proto3')



_CUSTOMERCONTEXT = DESCRIPTOR.message_types_by_name['CustomerContext']
CustomerContext = _reflection.GeneratedProtocolMessageType('CustomerContext', (_message.Message,), {
  'DESCRIPTOR' : _CUSTOMERCONTEXT,
  '__module__' : 'customer_context_pb2'
  # @@protoc_insertion_point(class_scope:processor.CustomerContext)
  })
_sym_db.RegisterMessage(CustomerContext)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _CUSTOMERCONTEXT._serialized_start=37
  _CUSTOMERCONTEXT._serialized_end=164
# @@protoc_insertion_point(module_scope)
