#!/bin/bash

PWD=$(pwd)
PROTOS=${PWD}/protos/
PROTOBUF=${PWD}/protobuf/

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}customer_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}payment_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}payment_instrument_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}printful_meta.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}processor_format_authorization_request_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}processor_format_customer_session_token_request_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}processor_format_payment_method_session_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}processor_format_refund_request_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}processor_parse_authorization_response_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}processor_parse_complete_payment_method_session_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}processor_parse_customer_session_token_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}processor_parse_payment_method_session_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}processor_parse_refund_request_context.proto

protoc -I=${PROTOS} --python_out=${PROTOBUF} ${PROTOS}primer-connect-context.proto
